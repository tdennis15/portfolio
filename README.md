# README #

Repository for the Portfolio Website of Travis (TJ) Dennis
This repository will hold the files of a ASP.net mvc 5 application
Master branch has continuous integration with travisdennis.azurewebsites.com

### What is this repository for? ###

This repository serves multiple purposes,
	1. It demonstrates basic version control and how I work in a iterative manner
	2. Supplies backup files to a website and hosts the upload of new features continuously.
	3. Allows for separation of implementation and development

### Who do I talk to? ###

If you would like to contact me please refer to the website contact page or through bitbucket. 
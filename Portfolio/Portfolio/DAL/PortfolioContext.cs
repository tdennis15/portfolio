namespace Portfolio.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class PortfolioContext : DbContext
    {
        public PortfolioContext()
            : base("name=PortfolioContext")
        {
        }

        public virtual DbSet<BlogPost> BlogPosts { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
        }
    }
}

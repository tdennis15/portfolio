﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Portfolio.Models;
using System.Net;

namespace Portfolio.Controllers
{
    public class BlogController : Controller
    {

        PortfolioContext db = new PortfolioContext();

        // GET: Blog
        public ActionResult Blog()
        {

            return View(db.BlogPosts.ToList());

        }
            // GET: Events/Details/5
        public ActionResult Details(int? id)
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                BlogPost @post = db.BlogPosts.Find(id);
                if (@post == null)
                {
                    return HttpNotFound();
                }
                return View(@post);
            }

        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(FormCollection form)
        {
            try
            {
                BlogPost post = db.BlogPosts.Create();

                post.PostContent = form["PostContent"];
                post.PostDate = DateTime.Now;
                db.BlogPosts.Add(post);
                db.SaveChanges();

                return RedirectToAction("Details", new { id = post.PostID });
            }
            catch
            {
                return View(Create());
            }
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            var post = db.BlogPosts.Where(a => a.PostID == id).FirstOrDefault();
            return View(post);
        }

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                var post = db.BlogPosts.Find(id);

                db.BlogPosts.Remove(post);
                db.SaveChanges();

                return RedirectToAction("Blog");
            }
            catch
            {
                return View();
            }
        }
    }
}
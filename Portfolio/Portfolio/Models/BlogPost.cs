namespace Portfolio.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("BlogPost")]
    public partial class BlogPost
    {
        [Key]
        public int PostID { get; set; }

        [Required]
        public string PostContent { get; set; }

        public DateTime PostDate { get; set; }
    }
}

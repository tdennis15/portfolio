﻿Create Table [dbo].[BlogPost] (
	[PostID]  Int Identity(1,1) Not Null,
	[PostContent] NVarchar(max) Not Null,
	[PostDate] DateTime Not Null,

Constraint [PK_dbo.BlogPost] Primary Key Clustered ([PostID] ASC)
);